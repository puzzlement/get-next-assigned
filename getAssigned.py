#!/usr/bin/env python3

import os, datetime as dt, sys, time
import json
from oauth2client.client import SignedJwtAssertionCredentials
import gspread

CREDFILE=os.path.expanduser('~/.gspreadcredentials')
SCOPE=['https://spreadsheets.google.com/feeds']

def get_date(st):
    fmts = ("%d/%m/%Y", "%d-%b-%Y")
    for fmt in fmts:
        try:
            return dt.datetime.strptime(st, fmt)
        except ValueError:
            pass
    raise ValueError("Could not parse date %s using any of formats %s" % (st, ", ".join(fmts)))

def get_credentials():
    json_key = json.load(open(CREDFILE))
    credentials = SignedJwtAssertionCredentials(json_key['client_email'],
            bytes(json_key['private_key'], 'utf-8'), SCOPE)
    return credentials

def end_of_day(t):
    return dt.datetime(t.year, t.month, t.day, 23, 59, 59)

class NoLaterDates(Exception): pass

def get_row(end_of_today, worksheet):
    dates = worksheet.col_values(1)

    found = False
    for count, value in enumerate(dates[1:]):
        row = count + 2
        line_date = end_of_day(get_date(worksheet.acell('A%d' % row).value))
        if line_date >= end_of_today:
            found = True
            break

    if found:
        return row
    else:
        raise NoLaterDates

def main():

    try:
        key = sys.argv[1]
    except IndexError:
        print ("Usage: getAssigned.py [spreadsheet key]", file=sys.stderr)
        sys.exit(1)
    today = end_of_day(dt.date.today())
    end_of_today = dt.datetime(today.year, today.month, today.day, 23, 59, 59)
    credentials = get_credentials()
    gc = gspread.authorize(credentials)
    ssh = gc.open_by_key(key)

    spammer = "UNASSIGNED"

    for year in end_of_today.year, end_of_today.year + 1:

        worksheet = ssh.worksheet(str(year))
        try:
            row = get_row(end_of_today, worksheet)
        except NoLaterDates:
            pass
        else:
            name_row = worksheet.row_values(row)
            for name in name_row[1:]:
                if name is not None and len(name.strip()) > 1:
                    spammer = name
                    break
            break # spammer found in year, no need to continue loop

    print(spammer)

def main_error_handling():
    found = False
    for i in range(5):
        try:
            main()
            found = True
            break
        except gspread.httpsession.HTTPError as e:
            print("Error trying to determine spammer, re-trying", file=sys.stderr)
            print(str(e), file=sys.stderr)
            time.sleep(10)

    if not found:
        print ("REPEATED ERRORS GETTING NAME, FAILING ENTIRELY", file=sys.stderr)

if __name__ == '__main__':
    main_error_handling()
