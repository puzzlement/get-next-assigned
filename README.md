get-next-assigned
=================

get-next-assigned fetches a name from a Google spreadsheet for a future task.

Use case
========

Your community uses a Google spreadsheet to sign up for tasks.

Each year has its own worksheet: "2015", "2016" and so on.

Column A in the worksheet has a date, eg "2015-01-01", "2015-01-02"

Column B has the assignee for that date, eg "Amaya", "Suzie", "Li Na".

The first row can be used for headings: values in it will be ignored.

The script will return the _next future_ assignee. That is, if run on Jan 1, it
will find the first assignee for any date on or after Jan 2 and print it on the
command line. It is useful for constructing reminders of who is abot to take
over a task or shift that they signed up for in a Google spreadsheet.

Installing get-next-assigned
============================

get-next-assigned requires Python 3. It also requires the Python modules listed in
requirements.txt

In order to access Google Drive's API, you will need to sign up for an API
credential [as explained in the gspread
documentation](https://github.com/burnash/gspread/blob/master/docs/oauth2.rst).

Place the JSON credential file downloaded from Google in ~/.gspreadcredentials.

You will need to share any spreadsheet you want get-next-assigned to access with the
email address given in the client\_email value of the API credential.

Running get-next-assigned
=========================

Pass the spreadsheet key (from the URL, ie
https://docs.google.com/spreadsheets/d/[GET THE KEY FROM HERE/edit) as a
command line argument to the getAssigned.py script, eg:

    ./getAssigned.py avpMSnkWZhxAS8r4uDp6GKhmhffavFmqdjIMYePJfKmq

About
=====

Credits
-------

Spam All the Links was developed by [Mary
Gardiner](http://mary.gardiner.id.au/) for the [Geek
Feminism](http://geekfeminism.org/) website.

Licence
-------

Spam All the Links is free software available under the MIT licence. See
LICENCE for full details.
